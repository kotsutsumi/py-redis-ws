import copy
import threading

import redis
import uvicorn as uvicorn
from fastapi import FastAPI
# noinspection PyPackageRequirements
from rediscluster import RedisCluster
# noinspection PyPackageRequirements
from starlette.websockets import WebSocket

# FastAPI app
app: FastAPI = FastAPI()

# WebSocket Clients
ws_clients = {}

# Redis Host
redis_host = "127.0.0.1"

# Redis port
redis_port = 6379

# Redis Cluster
redis_cluster = False

# Channel Name
redis_channel = "default"

# Redis Server Connection
redis_connection = None

# Redis Database Number
redis_db = 0

# WebSocket Clients
clients = {}

# Stock Redis Messages
messages = []

# Pub/Sub Redis Object
pubsub = None


def get_connection():
    # use global params
    global redis_connection, redis_host, redis_host, redis_cluster, redis_db

    if redis_connection is None:

        # create redis connector
        if redis_cluster:

            print(
                f"[connect][cluster] - host: {redis_host}, port: {redis_port}"
            )

            startup_nodes = [{"host": redis_host, "port": redis_port}]

            r = RedisCluster(startup_nodes=startup_nodes, decode_responses=True)

        else:

            r = redis.Redis(host=redis_host, port=redis_port, db=redis_db)

            print(f"[connect] - host: {redis_host}, port: {redis_port}")

            redis_connection = r

        return r

    return redis_connection


@app.websocket("/")
async def websocket_endpoint(ws: WebSocket):
    # use globals
    global clients, messages

    # accept WebSocket
    await ws.accept()

    # get client id
    key = ws.headers.get('sec-websocket-key')

    # add client's websocket object
    clients[key] = ws

    try:
        while True:

            # receive text
            data = await ws.receive_text()

            print(f"[receive] - client_key: {key}, msg: {data}")

            if redis_cluster:

                # set text
                get_connection().set("__redist_ws__", data)

                print(f"[set] - {data}")

                # append text
                messages.append(get_connection().get("__redist_ws__"))

            else:
                # publish
                get_connection().publish(redis_channel, data)

            send_messages = copy.copy(messages)
            messages = []

            for index, message in enumerate(send_messages):
                for client_key in clients.keys():
                    print(f"[send] - client_key: {client_key}, msg: {message}")

                    # send text
                    await clients[client_key].send_text(message)

    except:

        # close WebSocket connection
        await ws.close()

        # delete client
        del clients[key]


def subscribe():
    # use global params
    global redis_channel, messages, pubsub

    # subscribe
    pubsub = get_connection().pubsub()
    pubsub.subscribe(redis_channel)

    print(f"[subscribe] - {redis_channel}")

    try:
        for message in pubsub.listen():
            receive_type = message['type']
            receive_channel = message['channel'].decode()

            if receive_type == 'message' and receive_channel == redis_channel:
                # decode text
                data = message['data'].decode()

                print(f"[subscribe] - {data}")

                # append text
                messages.append(data)

    except:
        pass


@app.on_event("shutdown")
def shutdown_event():
    global pubsub

    # pubsub
    try:
        get_connection().close()
        pubsub.close()
    except:
        pass


class RedisWS:

    @staticmethod
    def run(port, _redis_host, _redis_port, _redis_cluster, _redis_channel,
            _redis_db):
        # use global params
        global redis_host, redis_port, redis_cluster, redis_channel, \
            redis_connection, redis_db

        # set Redis Host
        redis_host = _redis_host

        # set Redis Port
        redis_port = _redis_port

        # set Redis Cluster
        redis_cluster = _redis_cluster

        # set Redis Channel
        redis_channel = _redis_channel

        # start subscribe thread
        threading.Thread(target=subscribe, args=[]).start()

        # launch FastAPI
        uvicorn.run(app, host="0.0.0.0", port=port)


if __name__ == '__main__':
    pass

# EOF
